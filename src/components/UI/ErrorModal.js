import React from "react";
import ReactDOM from "react-dom";

import "./ErrorModal.css";
import Card from "./Card";

const Backdrop = (props) => {
  return <div className="backdrop" onClick={props.onConfirm}></div>;
};

const ModalOverlay = (props) => {
  return (
    <Card className="modal">
      <header className="modal_header">
        <h2>{props.title}</h2>
      </header>
      <div className="modal_content">
        <p>{props.message}</p>
      </div>
      <footer className="modal_actions">
        <button onClick={props.onConfirm}>Okay</button>
      </footer>
    </Card>
  );
};

const ErrorModal = (props) => {
  return (
    <React.Fragment>
      {ReactDOM.createPortal(
        <Backdrop onConfirm={props.onConfirm} />,
        document.getElementById("backdrop-root")
      )}
      {ReactDOM.createPortal(
        <ModalOverlay
          title={props.title}
          message={props.message}
          onConfirm={props.onConfirm}
        />,
        document.getElementById('overlay-root')
      )}
    </React.Fragment>
  );
};

export default ErrorModal;
