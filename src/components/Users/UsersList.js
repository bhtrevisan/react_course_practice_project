import "./UsersList.css";
import Card from "../UI/Card";

import User from "./User";

const UsersList = (props) => {
  console.log("Props:", props);

  if (props.items.length === 0) return <h2 className="user">No users found</h2>;

  return (
    <Card className="user-list">
      <ul>
        {props.items.map((elem) => (
          <User userName={elem.name} userAge={elem.age} />
        ))}
      </ul>
    </Card>
  );
};

export default UsersList;
