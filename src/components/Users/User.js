import "./User.css";
import Card from "../UI/Card";

const User = (props) => {
  console.log("props no User", props);
  return (
    <li className="user">
      <p>
        <b>Name:</b> {props.userName} <b>Age:</b> {props.userAge}{" "}
      </p>
    </li>
  );
};

export default User;
