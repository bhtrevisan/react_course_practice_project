import React, { useState } from "react";
import "./AddUser.css";
import ErrorModal from "../UI/ErrorModal";
import Wrapper from "../Helpers/Wrapper";

const AddUser = (props) => {
  const [enteredName, setEnteredName] = useState("");
  const [enteredAge, setEnteredAge] = useState("");
  const [error, setError] = useState("");

  const nameChangeHandler = (event) => {
    setEnteredName(event.target.value);
  };

  const ageChangeHandler = (event) => {
    setEnteredAge(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();

    // Validation of entered data
    if (enteredName.trim().length === 0) {
      setError({
        title: "Invalid name",
        message: `Please enter a valid name (can't be empty)`,
      });
      return;
    }

    if (enteredAge.trim().length === 0) {
      setError({
        title: "Invalid age",
        message: `Please enter a valid age (can't be empty)`,
      });
      return;
    }

    if (+enteredAge < 1) {
      setError({
        title: "Invalid age",
        message: `Please enter an age greater than 0`,
      });
      return;
    }

    const userData = {
      name: enteredName,
      age: enteredAge,
    };

    // Call function to pass data
    props.onAddUser(userData);

    setEnteredName("");
    setEnteredAge("");
  };

  const errorHandler = () => {
    setError(null);
  };

  return (
    <React.Fragment>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <form onSubmit={submitHandler}>
        <div className="new-user">
          <div className="new-user__controls">
            <div className="new-user__control">
              <label>Name</label>
              <input
                id="userName"
                type="text"
                placeholder="Name"
                value={enteredName}
                onChange={nameChangeHandler}
              ></input>
            </div>
            <div className="new-user__control">
              <label>Age</label>
              <input
                id="age"
                type="number"
                placeholder="Age"
                value={enteredAge}
                onChange={ageChangeHandler}
              ></input>
            </div>
            <div>
              <button className="new-user__actions" type="submit">
                Add User
              </button>
            </div>
          </div>
        </div>
      </form>
    </React.Fragment>
  );
};

export default AddUser;
